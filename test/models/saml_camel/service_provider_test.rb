require 'test_helper'

class SamlCamel::ServiceProviderTest < ActiveSupport::TestCase

  # mock application constants
  json_file = "#{SamlCamel::Engine.root}/test/models/saml_camel/settings.json"
  SamlCamel::ServiceProvider::SP_SETTINGS = JSON.parse(File.read(json_file))
  SamlCamel::ServiceProvider::SP_DEBUG = false
  SamlCamel::Logging::SHOULD_LOG = false

  SamlCamel::Transaction::SP_SETTINGS = JSON.parse(File.read(json_file))
  SamlCamel::Transaction::IDP_CERT = File.read("#{SamlCamel::Engine.root}/test/dummy/config/saml/development/idp_certificate.crt")
  SamlCamel::Transaction::SP_CERT = File.read("#{SamlCamel::Engine.root}/test/dummy/config/saml/development/saml_certificate.crt")
  SamlCamel::Transaction::SP_KEY = File.read("#{SamlCamel::Engine.root}/test/dummy/config/saml/development/saml_key.key")

  #mock saml session to run tests
  setup do
    lifetime = 3
    @cache_permit_key = "testing-samlCamel"
    Rails.cache.fetch(@cache_permit_key, expires_in: lifetime.hours) do
      { ip_address: '123.456.789', redirect_url: "localhost:3000/testSaml", session_start_time: Time.now }
    end
    @sp = SamlCamel::ServiceProvider.new(cache_permit_key: @cache_permit_key)
  end

  test 'check_expired_session' do
    assert @sp.check_expired_session(Time.now.to_s) #test that it can be string or time 1.0.6
    assert @sp.check_expired_session((Time.now - 2.hour))
    refute @sp.check_expired_session((Time.now - 4.hour).to_s) # timeout
    refute @sp.check_expired_session((Time.now - 10.hour).to_s) # lifetime
  end

  test 'duplicate_response_id' do
    Rails.cache.delete('saml_camel_response_ids')
    assert @sp.duplicate_response_id?("123")
    assert_raises(RuntimeError) { @sp.duplicate_response_id?("123") }
    assert @sp.duplicate_response_id?("456")
    Rails.cache.delete('saml_camel_response_ids')
  end

  test 'generate_saml_request' do
    temp_data = {remote_ip: "123.456.789", url: "localhost:3000/testSaml"}
    host_request = OpenStruct.new(temp_data)
    assert @sp.generate_saml_request(host_request)
  end

  test 'set_saml_session_lifetime' do
    @sp.set_saml_session_lifetime
    assert Time.now > Rails.cache.fetch(@cache_permit_key)[:session_start_time]
  end

  # TODO need to set down and determine best way to mock a saml response
  # without hitting the IDP
  test 'validate_idp_response' do
    skip
    @sp.validate_idp_response
  end

  test 'validate_ip' do
    assert @sp.validate_ip('123.456.789')
    refute @sp.validate_ip('000.000.000')
  end

  test 'validate_sp_session' do
    sp = SamlCamel::ServiceProvider.new(cache_permit_key: @cache_permit_key)
    assert sp.validate_sp_session(Time.now.to_s, '123.456.789')

    refute sp.validate_sp_session(Time.now.to_s, '000.00.780')
    refute sp.validate_sp_session((Time.now - 4.hour).to_s, '123.456.789')
    refute sp.validate_sp_session((Time.now - 4.hour).to_s, '000.00.780')
  end

  # TODO need to set down and determine best way to mock a saml response
  # without hitting the IDP
  test 'verify_sha_type' do
    skip
    assert @sp.verify_sha_type(response)
  end
end
