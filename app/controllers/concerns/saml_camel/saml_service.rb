# frozen_string_literal: true

require_dependency 'saml_camel/application_controller'

# Helper Methods for SamlController
module SamlCamel::SamlService # rubocop:disable Style/ClassAndModuleChildren
  extend ActiveSupport::Concern


  def cache_available?(app_cache)
    if app_cache
      true
    else
      session[:sp_session] = nil
      false
    end
  end

  # TODO: refactor
  def saml_protect(relay_state: nil) # rubocop:disable Metrics/MethodLength, Metrics/AbcSize:
    relay_state = relay_state ? "&RelayState=#{CGI.escape(relay_state)}" : ""
    #TODO move this
    begin
      settings = JSON.parse(File.read("config/saml/#{Rails.env}/settings.json"))
      sp_debug = settings['settings']['debug']
    rescue StandardError # rubocop:disable Lint/HandleExceptions
      # rescue othewise the generator fails
    end

    user_cache = cache_available?(Rails.cache.fetch(session[:saml_session_id])) if session[:saml_session_id]

    #user has an active saml_session_id and cache was found using that session id
    if session[:saml_session_id] && user_cache
      SamlCamel::Logging.debug('Saml Session and User Cache Found.') if sp_debug
      SamlCamel::Logging.debug("SAML session id: #{session[:saml_session_id]} | Cache: #{user_cache}") if sp_debug
      sp = SamlCamel::ServiceProvider.new(
        cache_permit_key: session[:saml_session_id].to_sym,
        saml_attributes: session[:saml_attributes]
      )
      session[:sp_session] = sp.validate_sp_session(session[:sp_session], request.remote_ip)

      # run this if a user does not have an sp session, or if the response was a failure
      unless session[:saml_response_success] || session[:sp_session]
        SamlCamel::Logging.debug('SAML response not successful or no sp session not valid. Generating new request.') if sp_debug
        SamlCamel::Logging.debug("SAML response: #{session[:saml_response_success]}") if sp_debug
        SamlCamel::Logging.debug("SP session #{session[:sp_session]}") if sp_debug
        
        session[:saml_session_id] = SamlCamel::ServiceProvider.generate_permit_key
        saml_request_url = SamlCamel::ServiceProvider.new(
          cache_permit_key: session[:saml_session_id].to_sym
        ).generate_saml_request(request)

        redirect_to(saml_request_url + relay_state)
      end

    # user did not have a saml_session_id and an active cache
    else
      SamlCamel::Logging.debug('User Cache or saml session id not found. Generating new request.') if sp_debug
      SamlCamel::Logging.debug("SAML session id: #{session[:saml_session_id]} | Cache: #{user_cache}") if sp_debug

      session[:saml_session_id] = SamlCamel::ServiceProvider.generate_permit_key
      saml_request_url = SamlCamel::ServiceProvider.new(
        cache_permit_key: session[:saml_session_id].to_sym
      ).generate_saml_request(request)
      redirect_to(saml_request_url + relay_state)
    end
    session[:saml_response_success] = nil # keeps us from looping
  end
end
