# frozen_string_literal: true

module SamlCamel
  # class serves as core buisness logic for SamlCamle SP
  class ServiceProvider
    attr_reader :cache_permit_key, :saml_attributes

    def initialize(cache_permit_key: nil, saml_attributes: nil)
      @cache_permit_key = cache_permit_key.try(:to_sym)
      @saml_attributes = saml_attributes
      @user_cache = Rails.cache.fetch(@cache_permit_key)
    end

    def self.generate_permit_key
      secure_random = SecureRandom.base64.chomp.gsub(/\W/, '')
      "samlCamel#{secure_random}"
    end

    def self.mock_saml_cache(permit_key: nil, ip_address: nil)
      lifetime = SP_SETTINGS['settings']['sp_session_lifetime']
      Rails.cache.fetch(@cache_permit_key, expires_in: lifetime.hours) do
        { ip_address: host_request.remote_ip }
      end
    end

    # ol OneLogin
    def self.ol_response(idp_response, raw_response: false)
      clock_drift = set_clock_drift
      settings = SamlCamel::Transaction.saml_settings(raw_response: raw_response)
      if clock_drift
        response          = OneLogin::RubySaml::Response.new(idp_response, settings: settings, allowed_clock_drift: clock_drift.second)
      else
        response          = OneLogin::RubySaml::Response.new(idp_response, settings: settings)
      end
      response.settings = settings
      response
    end

    #if user configured clock drift, check configuration
    # ruby saml default I "think" is 180 sec based of the java saml pull request https://github.com/onelogin/java-saml/issues/89
    # however when I pulled the ruby-saml gem and searched the repo it looks like there is no clock drift by default
    def self.set_clock_drift
      clock_drift = SP_SETTINGS.dig('settings','clock_drift')
      # clock drift must either be an integer of falsey, classes have come through
      # differently (fixnum, bignum, integer), using a regex instead to see if that
      # addresses some issues https://stackoverflow.com/questions/16774064/regular-expression-for-whole-numbers-and-integers
      unless !clock_drift || clock_drift.match(/(?<![-.])\b[0-9]+\b(?!\.[0-9])/)
        SamlCamel::Logging.clock_drift(clock_drift)
        raise "Clock Drift Incorrectly Configured."
      end
      clock_drift.to_i
    end


    # TODO: method too complex
    def check_expired_session(sp_session) # rubocop:disable Metrics/MethodLength, Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity, Metrics/AbcSize, Metrics/LineLength
      sp_timeout = SP_SETTINGS['settings']['sp_session_timeout']
      sp_lifetime = SP_SETTINGS['settings']['sp_session_lifetime']

      set_saml_session_lifetime if @user_cache[:session_start_time].nil?
      sp_session_init_time = @user_cache[:session_start_time]

      SamlCamel::Logging.debug('Checking if session expired') if SP_DEBUG
      ######## set session[:sp_session] maybe a seperate method
      if sp_session
        # if the session has exceeded the allowed lifetime, remove session
        if (Time.now - sp_session_init_time) > sp_lifetime.hour
          SamlCamel::Logging.debug('Session has exceeded the allowed lifetime.') if SP_DEBUG
          SamlCamel::Logging.debug("Current Time: #{Time.now} | Session Init: #{ sp_session_init_time}") if SP_DEBUG
          SamlCamel::Logging.expired_session(@saml_attributes)
          return nil
        end

        # if the session has timed out remove session, otherwise refresh
        sp_session = sp_session.to_s if sp_session.class != String
        if (Time.now - Time.parse(sp_session)) < sp_timeout.hour
          SamlCamel::Logging.debug('Session within timeout, session renewed') if SP_DEBUG
          Time.now
        else
          SamlCamel::Logging.expired_session(@saml_attributes)
          return nil
        end
      else # if no sp session return nil
        SamlCamel::Logging.debug('No session found when checking expiration') if SP_DEBUG
        return nil
      end
    end

    # TODO: method too complex
    def duplicate_response_id?(response_id, count: 3) # rubocop:disable Metrics/MethodLength, Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity, Metrics/AbcSize, Metrics/LineLength
      SamlCamel::Logging.debug('Checking uniqueness of response id') if SP_DEBUG

      # use semaphore to only allow 1 thread at a time to access
      @semaphore ||= Mutex.new
      @semaphore.synchronize { #  rubocop:disable Style/BlockDelimiters
        ids = Rails.cache.fetch('saml_camel_response_ids')
        if ids
          if ids.include?(response_id)
            SamlCamel::Logging.debug('Response id has already been used') if SP_DEBUG
            raise 'SAML response ID already issued.'
          else
            SamlCamel::Logging.debug("Unique Response ID #{response_id}") if SP_DEBUG
            ids << response_id
            Rails.cache.fetch('saml_camel_response_ids', expires_in: 1.hours) do
              ids
            end
          end
        else
          SamlCamel::Logging.debug("Unique Response ID #{response_id}") if SP_DEBUG
          Rails.cache.fetch('saml_camel_response_ids', expires_in: 1.hours) do
            [response_id]
          end
        end
      }
    rescue ThreadError
      puts 'locked ' * 50
      # SamlCamel::Logging.debug('Response ID check locked, trying again') if SP_DEBUG
      if count.positive? # rubocop:disable Style/GuardClause
        sleep(0.1)
        duplicate_response_id?(response_id, count: count - 1)
      else raise 'Resposne ID Validation Error'
      end
    end

    # generates a saml requests and establishes a cache for the user
    def generate_saml_request(host_request, force_authn: false)
      SamlCamel::Logging.debug("Creating request for #{host_request.remote_ip}") if SP_DEBUG
      request = OneLogin::RubySaml::Authrequest.new
      lifetime = SP_SETTINGS['settings']['sp_session_lifetime']

      # store ip address and original url request in memory to be used for
      # verification and redirect after response
      Rails.cache.fetch(@cache_permit_key, expires_in: lifetime.hours) do
        { ip_address: host_request.remote_ip, redirect_url: host_request.url }
      end
      request.create(SamlCamel::Transaction.saml_settings(raw_response: force_authn))
    end



    # set saml_session lifetime, called if none set
    # TODO: this may need to be renamed, it's not really setting the lifetime
    # it's refreshing the last time a user authenticated
    def set_saml_session_lifetime
      user_saml_cache = Rails.cache.fetch(@cache_permit_key)
      user_saml_cache[:session_start_time] = Time.now
      sp_lifetime = SP_SETTINGS['settings']['sp_session_lifetime']

      SamlCamel::Logging.debug("Setting lifetime of session. Lifetime of #{sp_lifetime} hours") if SP_DEBUG
      Rails.cache.write(@cache_permit_key, user_saml_cache, expires_in: sp_lifetime.hours)
    end

    # NOTE these methods will raise errors if not a valid response_id
    # which in turn will trigger a resuce that kills the sp session
    def validate_idp_response(response, remote_ip)
      SamlCamel::Logging.debug('Validating IDP response') if SP_DEBUG
      if response.is_valid?
        # validate not sha1
        verify_sha_type(response)

        # validate IP address
        raise 'IP mismatch error' unless validate_ip(remote_ip)

        response_id = response.id(response.document)
        duplicate_response_id?(response_id)

        response
      else
        false
      end
    end

    # validate that ip address has not changed
    def validate_ip(remote_ip)
      if SP_DEBUG
        SamlCamel::Logging.debug(
          "Validating IP consistency. IP @ request = #{@user_cache[:ip_address]}
 | current IP = #{remote_ip}"
        )
      end

      return true if (!SP_SETTINGS['settings']['validate_ip'] || remote_ip == @user_cache[:ip_address] )
      SamlCamel::Logging.bad_ip(@saml_attributes,
                                @user_cache[:ip_address],
                                remote_ip)
      nil
    end

    # validates a the sp timestamps and ip. returns a new timestamp if
    # everything is good otherwise returns nil

    def validate_sp_session(sp_session, remote_ip)
      new_session = check_expired_session(sp_session)
      valid_ip = validate_ip(remote_ip) if new_session
      return new_session if new_session && valid_ip
    end

    def verify_sha_type(response)
      SamlCamel::Logging.debug('Verify response is not SHA1') if SP_DEBUG

      # was suggested to use an xml parser, however was having great difficulyt
      # with nokogiri open to trying a different parser or advice on nokogiri as
      # it's not reacting as I would typically expect it to
      raw_xml_string = response.decrypted_document.to_s
      attr_scan = raw_xml_string.scan(%r{<ds:SignatureMethod.*\/>})
      is_sha1 = attr_scan[0].match('sha1')

      raise 'SHA1 algorithm not supported' if is_sha1
    end
  end
end
